package utills;

import user.account.UserProfile;
import java.util.ArrayList;
import java.util.Scanner;
import static utills.UtilMethodes.*;

public class Menu {

    public static ArrayList<UserProfile> user_profiles = Login.loadInfo();

    public static void mainMenu() {


        char menumovement = 'o' ;

        do {
            System.out.println("Welcome to Command Line TWEETER!");

            menumovement = MainMenuGraphics(menumovement);

            switch (menumovement)
            {
                case '1':

                    Login.login();

                    break;
                case '2':

                    clearScreen();
                    signUp(user_profiles);
                    break;
                case '3':

                    System.exit(0);
            }
        } while (true);

    }//end mainMenu

    public static char MainMenuGraphics( char option)
    {
        Scanner getter = new Scanner(System.in);

        String[][] arr = {{"\u250c","\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500","\u2510"},
                {"\u2502","          LogIn         ","\u2502"},
                {"\u251c","\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500","\u2524"},
                {"\u2502","          SignUp        ","\u2502"},
                {"\u251c","\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500","\u2524"},
                {"\u2502","          Quit          ","\u2502"},
                {"\u2514","\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500","\u2518"}};

        String Line = "\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550";
        int satr = 1 , soton = 1;
        char tap = 'o';

        String[][] temp = new String[7][3];
        for(int p1=0 ; p1<7 ; p1++)
            for(int p2=0 ; p2<3 ; p2++)
                temp[p1][p2]=arr[p1][p2];

        temp[satr-1][soton] = Line;
        temp[satr+1][soton] = Line;


        temp[satr][soton-1] = "\u2551"  ;

        temp[satr][soton+1] = "\u2551"  ;

        temp[satr-1][soton-1] = "\u2554";
        temp[satr+1][soton+1] = "\u255d";
        temp[satr-1][soton+1] = "\u2557";
        temp[satr+1][soton-1] = "\u255a";

        PrintMainMenu(temp);

        while(true)
        {
            //String[][] temp = new String[13][3];
            for(int p1=0 ; p1<7 ; p1++)
                for(int p2=0 ; p2<3 ; p2++)
                    temp[p1][p2]=arr[p1][p2];    //temp str


            char action = getter.next().charAt(0);
            clearScreen();

            if(action=='w' || action=='a')
            {
                if ( satr == 1 ){satr=5;}
                else if ( satr == 3 ){satr=1;}
                else if ( satr == 5 ){satr=3;}

            }

            else if(action=='s' || action=='d')
            {
                if ( satr == 1 ){satr=3;}
                else if ( satr == 3 ){satr=5;}
                else if ( satr == 5 ){satr=1;}

            }

            else if(action=='y')
            {
                if (satr == 1 ){tap = '1';}
                else if (satr == 3 ){tap = '2';}
                else if (satr == 5 ){tap = '3';}


                option = tap;
                break;

            }
            temp[satr-1][soton] = Line;
            temp[satr+1][soton] = Line;
            temp[satr][soton-1] = "\u2551"  ;
            temp[satr][soton+1] = "\u2551"  ;
            temp[satr-1][soton-1] = "\u2554";
            temp[satr+1][soton+1] = "\u255d";
            temp[satr-1][soton+1] = "\u2557";
            temp[satr+1][soton-1] = "\u255a";

            PrintMainMenu(temp);
        }
        return option;
    }

    public static void PrintMainMenu(String[][] Arr)
    {
        for(int i = 0 ; i < 7 ; i++){
            for (int j = 0 ; j < 3 ; j++)
                System.out.print(Arr[i][j]);
            System.out.println();
        }
    }




}

