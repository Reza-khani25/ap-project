package utills;

import user.account.UserProfile;
import user.twitts.Tweet;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Scanner;

import static utills.UtilMethodes.clearScreen;


public class Login extends UtilMethodes{

    public static void login() {
        ArrayList<UserProfile> user_profiles = loadInfo();
        UserProfile loggedUser ;
        try { loggedUser = getUserName(user_profiles);  }
        catch (WrongInfoException e){
            System.out.println("this userName does not exist . sign up first");
            Menu.mainMenu();
            return;
             }

        if(isTruePass(loggedUser)){
            System.out.println("welcome " + loggedUser.getUSERNAME());
            homePage loged = new homePage(loggedUser , user_profiles);
            loged.homePageMenu();

             }
        else {
            System.out.println("wrong password! ");
            Menu.mainMenu();
             }

    } // end login


    public static UserProfile getUserName(ArrayList<UserProfile> user_profiles) {


        System.out.println("please enter your username (enter \"cancel\" to go back to menu)");
        String user_name = scanner.next();

        if (user_name.equals("cancel"))
            Menu.mainMenu();
        else {
            for (UserProfile u : user_profiles) {
                if (u.getUSERNAME().equals(user_name)) {
                    //user_profiles.remove(u); // add
                    // Login.saveUserProfiles(user_profiles); //add
                    return u;

                }
            }
        }

        throw new WrongInfoException();
     } //end get UserName

    public static boolean isTruePass(UserProfile u)  {

        System.out.println("please enter your password!");
        String password = scanner.next();

        return u.getPASSWORD().equals(password);
    } // end get password



    public static ArrayList<UserProfile> loadInfo(){

        ArrayList<UserProfile> user_profiles;
        try(FileInputStream f1 = new FileInputStream("userProfiles");
            ObjectInputStream in = new ObjectInputStream(f1);){

             user_profiles = (ArrayList<UserProfile>) in.readObject();
        }catch (IOException | ClassNotFoundException e){
             user_profiles = new ArrayList<>();
          }

        return user_profiles;
    } // end load info


 }// end login class






  class homePage implements homePageInterface{

    private UserProfile user ;
    ArrayList<UserProfile> user_profiles ;
    Scanner scanner = new Scanner(System.in);


    public void homePageMenu(){


        char menumovement = 'o' ;

        do {
            //System.out.println("Welcome " + user.getUSERNAME());
            System.out.println("Chose what you want to do:");

            menumovement = HomePageMenuGraphics(menumovement);

            switch (menumovement)
            {
                case '1':
                    myProfile();
                    break;
                case '2':
                    tweet();
                    break;
                case '3':
                    reTweet();
                    break;
                case '4':
                    followers();
                    break;
                case '5':
                    followings();
                    break;
                case '6':
                    profile();
                    break;
                case '7':
                    like();
                    break;
                case '8':
                    follow();
                    break;
                case '9':
                    unFollow();
                    break;
                case 'A':
                    timeLine();
                    break;
                case 'B':
                    logOut();
                    break;
                case 'C':
                    System.exit(0);
            }
        } while (true);

    }//end HomeMenu




      public static char HomePageMenuGraphics( char option)
      {
          Scanner getter = new Scanner(System.in);

          String[][] arr = {{"\u250c","\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500","\u2510"},
                  {"\u2502","        My Profile      ","\u2502"},
                  {"\u251c","\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500","\u2524"},
                  {"\u2502","          Tweet!        ","\u2502"},
                  {"\u251c","\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500","\u2524"},
                  {"\u2502","          ReTweet       ","\u2502"},
                  {"\u251c","\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500","\u2524"},
                  {"\u2502","         Followers      ","\u2502"},
                  {"\u251c","\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500","\u2524"},
                  {"\u2502","         Following      ","\u2502"},
                  {"\u251c","\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500","\u2524"},
                  {"\u2502","         Profiles       ","\u2502"},
                  {"\u251c","\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500","\u2524"},
                  {"\u2502","           Like         ","\u2502"},
                  {"\u251c","\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500","\u2524"},
                  {"\u2502","          Follow        ","\u2502"},
                  {"\u251c","\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500","\u2524"},
                  {"\u2502","         UnFollow       ","\u2502"},
                  {"\u251c","\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500","\u2524"},
                  {"\u2502","         TimeLine       ","\u2502"},
                  {"\u251c","\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500","\u2524"},
                  {"\u2502","          LogOut        ","\u2502"},
                  {"\u251c","\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500","\u2524"},
                  {"\u2502","          Quit          ","\u2502"},
                  {"\u2514","\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500","\u2518"},
                  {"\u2514","\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500","\u2518"}};

          String Line = "\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550\u2550";


          int satr = 1 , soton = 1;
          char tap = 'o';

          String[][] temp = new String[25][3];
          for(int p1=0 ; p1<25 ; p1++)
              for(int p2=0 ; p2<3 ; p2++)
                  temp[p1][p2]=arr[p1][p2];

          temp[satr-1][soton] = Line;
          temp[satr+1][soton] = Line;


          temp[satr][soton-1] = "\u2551"  ;

          temp[satr][soton+1] = "\u2551"  ;

          temp[satr-1][soton-1] = "\u2554";
          temp[satr+1][soton+1] = "\u255d";
          temp[satr-1][soton+1] = "\u2557";
          temp[satr+1][soton-1] = "\u255a";

          PrintHomePageMainMenu(temp);

          while(true)
          {
              //String[][] temp = new String[13][3];
              for(int p1=0 ; p1<24 ; p1++)
                  for(int p2=0 ; p2<3 ; p2++)
                      temp[p1][p2]=arr[p1][p2];    //temp str


              char action = getter.next().charAt(0);
              clearScreen();

              if(action=='w' || action=='a' )
              {
                  if ( satr == 1 ){satr=23;}
                  else if ( satr == 3 ){satr=1;}
                  else if ( satr == 5 ){satr=3;}
                  else if ( satr == 7 ){satr=5;}
                  else if ( satr == 9 ){satr=7;}
                  else if ( satr == 11 ){satr=9;}
                  else if ( satr == 13 ){satr=11;}
                  else if ( satr == 15 ){satr=13;}
                  else if ( satr == 17 ){satr=15;}
                  else if ( satr == 19 ){satr=17;}
                  else if ( satr == 21 ){satr=19;}
                  else if ( satr == 23 ){satr=21;}

              }

              else if(action=='s' || action=='d' )
              {
                  if ( satr == 1 ){satr=3;}
                  else if ( satr == 3 ){satr=5;}
                  else if ( satr == 5 ){satr=7;}
                  else if ( satr == 7 ){satr=9;}
                  else if ( satr == 9 ){satr=11;}
                  else if ( satr == 11 ){satr=13;}
                  else if ( satr == 13 ){satr=15;}
                  else if ( satr == 15 ){satr=17;}
                  else if ( satr == 17 ){satr=19;}
                  else if ( satr == 19 ){satr=21;}
                  else if ( satr == 21 ){satr=23;}
                  else if ( satr == 23 ){satr=1;}

              }

              else if(action=='y')
              {
                  if (satr == 1 ){tap = '1';}
                  else if (satr == 3 ){tap = '2';}
                  else if (satr == 5 ){tap = '3';}
                  else if (satr == 7 ){tap = '4';}
                  else if (satr == 9 ){tap = '5';}
                  else if (satr == 11 ){tap = '6';}
                  else if (satr == 13 ){tap = '7';}
                  else if (satr == 15 ){tap = '8';}
                  else if (satr == 17 ){tap = '9';}
                  else if (satr == 19 ){tap = 'A';}
                  else if (satr == 21 ){tap = 'B';}
                  else if (satr == 23 ){tap = 'C';}


                  option = tap;
                  break;

              }
              temp[satr-1][soton] = Line;
              temp[satr+1][soton] = Line;
              temp[satr][soton-1] = "\u2551"  ;
              temp[satr][soton+1] = "\u2551"  ;
              temp[satr-1][soton-1] = "\u2554";
              temp[satr+1][soton+1] = "\u255d";
              temp[satr-1][soton+1] = "\u2557";
              temp[satr+1][soton-1] = "\u255a";

              PrintHomePageMainMenu(temp);
          }
          return option;
      }

      public static void PrintHomePageMainMenu(String[][] Arr)
      {
          for(int i = 0 ; i < 25 ; i++){
              for (int j = 0 ; j < 3 ; j++)
                  System.out.print(Arr[i][j]);
              System.out.println();
          }
      }



    @Override
    public void logOut() {Menu.mainMenu(); };

    @Override
    public void tweet() {
        System.out.println(user.getUSERNAME() + " please enter your tweet: ");
        System.out.println("NOTE : enter ( END ) in new line when done !");

        String caption = "" , line = "";

        while(!line.equals("END")) {
            caption = caption + "\n" + line;
            line = scanner.nextLine();
        }


        Tweet tweet = new Tweet(user.getUSERNAME() , caption);
        user.sendTweet(tweet);
        System.out.println("tweet sent !");

        Login.saveUserProfiles(user_profiles);
        homePageMenu();
    } // end tweet

      @Override
      public void reTweet()  {

          boolean valliTweetId = false ;

          System.out.println("enter a tweet id to retweet :");
          String tweetId = scanner.next();

          String[] splitted = tweetId.split("@") ;

          for(UserProfile u : user_profiles)
          {
              if(u.getUSERNAME().equals(splitted[0]))
              {
                  for(Tweet t : u.getAllTweets())
                  {
                      if(t.getTweetID().equals(tweetId))
                      {
                          String caption = "retweeted from " + u.getUSERNAME() + "\n" ;
                          caption += t.getCaption() ;
                          Tweet tweet = new Tweet(user.getUSERNAME() , caption) ;
                          user.sendTweet(tweet);

                          valliTweetId = true ;
                          break;
                      }

                  }
              }
          }

          if(valliTweetId)
          {
              System.out.println("post retweeted ");
              scanner.next();
              Login.saveUserProfiles(user_profiles);
              homePageMenu();
          }
          else
          {
              System.out.println("wrong tweet id ! , enter a key to go back to menu");
              scanner.next();
          }

          homePageMenu();



      } // end retweet

    @Override
    public void follow()  {
        System.out.print("signed up users are : ");
        print(user_profiles , user);
        System.out.println("enter an userName to follow :");
        String newFollowing = scanner.next();

        boolean trueUsername = false ;

        for(UserProfile u : user_profiles){
            if(u.getUSERNAME().equals(newFollowing))
            {
                user.addFollowing(u);
                u.addFollower(user);
                System.out.println("you successfully followed " + u.getUSERNAME());
                trueUsername = true ;
            }
        }

        if(trueUsername)
        {
            Login.saveUserProfiles(user_profiles);
            homePageMenu();
        }
        else
        {
            System.out.println("cant find the user.");
            follow();
        }

    }

    @Override
    public void unFollow()  {
        System.out.print("you follow : " );

        print(user.getFollowings());

        System.out.println("enter a userName to unFollow");
        String removedFollowing = scanner.next();

        boolean trueUsername = false;

        for(UserProfile u : user.getFollowings())
        {
            if(u.getUSERNAME().equals(removedFollowing))
            {

                user.removeFollowing(u);
                u.removeFollower(user);
                System.out.println("you successfully unFollowed " + u.getUSERNAME());

                trueUsername = true;
                break;

            }
        }

        if(trueUsername)
            Login.saveUserProfiles(user_profiles);

        else
            System.out.println("cant find the user.");

        homePageMenu();


    } // end unfollow

     @Override
     public void followers()  {
         System.out.print("your followers : ");
         print(user.getFollowers());
         System.out.println("enter a key to go back to menu");
         scanner.next();
         homePageMenu();
     }

     @Override
     public void followings()  {
         System.out.print("you follow : ");
         print(user.getFollowings());
         System.out.println("enter a key to go back to menu");
         scanner.next();
         homePageMenu();
     }


     @Override
     public void myProfile()  {
         System.out.println("----- " + user.getUSERNAME() + "'s profile -----");
         UtilMethodes.sortTweets(user.getAllTweets());
         UtilMethodes.showTweets(user.getAllTweets());

         System.out.println("enter a key to go back to menu");
         scanner.next();
         homePageMenu();
     }


     @Override
     public void profile()  {
         System.out.print("signed up users are : ");
         print(user_profiles , user);
         System.out.println("enter a userName to see his/her tweets :");
         String who = scanner.next();
         boolean valliUsername = false;

         for(UserProfile u : user_profiles)
         {
             if(u.getUSERNAME().equals(who))
             {
                 UtilMethodes.sortTweets(u.getAllTweets());
                 UtilMethodes.showTweets(u.getAllTweets());
                 valliUsername =true;
             }
         }

         if(valliUsername)
         {
             System.out.println("enter a key to go back to menu");
             scanner.next();
             homePageMenu();
         }
         else
         {
             System.out.println("no user found");
             homePageMenu();
         }
     }

     @Override
     public void timeLine(){

        ArrayList<Tweet> allTweets = UtilMethodes.joinTweets(user) ;
        UtilMethodes.sortTweets(allTweets);
        UtilMethodes.showTweets(allTweets);
         System.out.println("enter a key to go back to menu");
         scanner.next();
         try {
             homePageMenu();
         } catch (Exception e) {
             e.printStackTrace();
         }
     }

     @Override
     public void like()  {

        boolean valliTweetId = false ;

         System.out.println("enter a tweet id to like :");
         String tweetId = scanner.next();

         String[] splitted = tweetId.split("@") ;

         for(UserProfile u : user_profiles)
         {
             if(u.getUSERNAME().equals(splitted[0]))
             {
                 for(Tweet t : u.getAllTweets())
                 {
                     if(t.getTweetID().equals(tweetId))
                     {
                         t.like();
                         valliTweetId = true ;
                         break;
                     }

                 }
             }
         }

         if(valliTweetId)
         {
             System.out.println("you like this tweet , enter a key to go back to menu");
             scanner.next();
             Login.saveUserProfiles(user_profiles);
             homePageMenu();
         }
         else
         {
             System.out.println("wrong tweet id ! , enter a key to go back to menu");
             scanner.next();
         }

         homePageMenu();
     } // end like



    static void print(ArrayList<UserProfile> arr)
    {
        for(UserProfile u : arr)
            System.out.print(u.getUSERNAME() + " ,");
        System.out.println("");
    }

    static void print(ArrayList<UserProfile> arr , UserProfile exception)
    {
        for(UserProfile u : arr)
        {
            if(!u.getUSERNAME().equals(exception.getUSERNAME()))
                 System.out.print(u.getUSERNAME() + " ,");
        }
        System.out.println("");
    }

    public homePage(UserProfile user , ArrayList<UserProfile> user_profiles)
    {
        this.user = user;
        this.user_profiles = user_profiles;
    }



}// end homePage class


