package utills;

import java.io.*;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Scanner;
import user.account.UserProfile;
import user.twitts.Tweet;


public class UtilMethodes {

    static Scanner scanner = new Scanner(System.in);

    public static void clearScreen(){
        for(int i = 0 ; i<30 ; i++)
            System.out.println(" ");
    }

    public static void ClearScreen() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }


    public static void signUp(ArrayList<UserProfile> user_profiles) {

        /**
         *
         * load the userNames that have already been signed
         */
        ArrayList<String> user_names ;
        try{
            FileInputStream f2 = new FileInputStream("userNames");
            ObjectInputStream in = new ObjectInputStream(f2);
            user_names = (ArrayList<String>) in.readObject();
            in.close();
            f2.close();
        }catch (IOException | ClassNotFoundException e){
            user_names = new ArrayList<>();
        }

        String userName ;

        // error for duplicate userNames
        while(true){

            try {
                System.out.println("\n please enter your username : (use only lowerCases and numbers)");
                userName = scanner.next();
                 if (user_names.contains(userName))
                      throw new DuplicateUserNameException();
                 else
                        break;
            } catch (DuplicateUserNameException e) {
                System.out.println("this userName is already used  by someOne else");
            }
        }


        System.out.println("\n please enter your password : (use only characters and numbers)");
        String passWord = scanner.next();


        UserProfile newUser = new UserProfile(userName , passWord) ;
        user_profiles.add(newUser);

        saveUserProfiles(user_profiles);

        System.out.println("signed up succesfully \n");


        user_names.add(userName) ;
        saveLoggedUserNames(user_names);
        System.out.println(user_names);



        Menu.mainMenu();

    }// end signUp

    public static void saveLoggedUserNames(ArrayList<String> user_names ){

        try{
            FileOutputStream f1 = new FileOutputStream("userNames");
            ObjectOutputStream out = new ObjectOutputStream(f1);
            out.writeObject(user_names);
            out.close();
            f1.close();
        }catch (IOException e){

        }

    } // end saveLoggedUserNames

    public static void saveUserProfiles(ArrayList<UserProfile> user_profiles){
       try {
        FileOutputStream f1 = new FileOutputStream("userProfiles");
        ObjectOutputStream out = new ObjectOutputStream(f1);
        out.writeObject(user_profiles);
        out.close();
        f1.close();
        }catch (IOException e){}
    }


    public static void sortTweets(ArrayList<Tweet> tweets){

        Collections.sort(tweets , (b , a)->a.getDate().compareTo(b.getDate()));
    }

    public static void showTweets(ArrayList<Tweet> tweets){
        for(Tweet t : tweets)
            t.showTweet();
    }

    /**
     *
     * @param user
     * @return all tweets for timeLine
     */
    public static ArrayList<Tweet> joinTweets(UserProfile user){

        ArrayList<Tweet> joined = new ArrayList<>();

        joined.addAll(user.getAllTweets());

        for(UserProfile u : user.getFollowings())
            joined.addAll(u.getAllTweets());

        return joined  ;

    }
}
