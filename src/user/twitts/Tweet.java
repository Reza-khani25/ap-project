package user.twitts;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.time.*;
import java.time.format.DateTimeFormatter;

public class Tweet implements Serializable {

    private String tweetID;
    private String caption ;
    private String username;
    private LocalDateTime date ;
    private int likes = 0;

    public String getTweetID() {
        return tweetID;
    }

    public String getCaption() {
        return caption;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public int getLikes() { return likes; }

    public void like(){likes++;}

    public void showTweet(){
        System.out.println("------------------------------");
        System.out.println("tweet id = " + tweetID);
        System.out.println("   by "+ "\"" +username + "\"" + " at " + date.format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss")));
        System.out.format("%d likes" , likes);
        System.out.println("\n" + caption + "\n");
        System.out.println("------------------------------");
    }

    public void setCaption(@NotNull String caption) {

        if(caption.length()>140)
            caption = caption.substring(0 , 140) ;

        this.caption = caption;
    }

    public void generateTweetId(String userName){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy@HH:mm:ss");
        this.tweetID = userName +"@" + this. date.format(formatter);
    }

    public Tweet(String userName , String caption){

        date = LocalDateTime.now();
        this.username = userName ;
        setCaption(caption);
        generateTweetId(userName);

    }
}
