package user.account;

import java.io.Serializable;
import java.util.ArrayList;


import user.twitts.*;

public class UserProfile implements Serializable , UserMethodes {

    private final String USERNAME , PASSWORD ;
    private ArrayList<Tweet> allTweets   = new ArrayList<>();
    private ArrayList<UserProfile> followers  = new ArrayList<>();
    private ArrayList<UserProfile> followings = new ArrayList<>();

    public String getUSERNAME() { return USERNAME; }

    public String getPASSWORD() { return PASSWORD; }

    public ArrayList<Tweet> getAllTweets() { return allTweets; }

    @Override
    public void sendTweet(Tweet tweet) { this.allTweets.add(tweet); }

    @Override
    public void addFollower(UserProfile newFollower){ this.followers.add(newFollower); }

    @Override
    public void addFollowing(UserProfile newFollowing){ this.followings.add(newFollowing); }

    public ArrayList<UserProfile> getFollowers() { return followers; }

    public ArrayList<UserProfile> getFollowings() { return followings; }

    public void removeFollowing(UserProfile u) { this.followings.remove(u); }

    public void removeFollower(UserProfile u) {this.followers.remove(u); }

    public UserProfile(String userName , String passWord){
        this.USERNAME = userName;
        this.PASSWORD = passWord;
    }



}
