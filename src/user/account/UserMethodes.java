package user.account;

import user.twitts.Tweet;

public interface UserMethodes {

    void sendTweet(Tweet tweet);

    void addFollower(UserProfile newFollower);

  // void removeFollower(UserProfile removedFollower);

    void addFollowing(UserProfile newFollowing);

  //  void removeFollowing(UserProfile removedFollowing);

}
